package practice10_19;

public class Practice11 {
	public static void main(String[] args) {

		System.out.println("入力された数値を合計します。1から9の数値を入力して下さい。0を入力したら計算結果を出力します。");
		int total = 0;
		while (true) {
			int num = new java.util.Scanner(System.in).nextInt();

			if (num == 0) {
				System.out.println("合計値は" + total + "です。");
				break;
			}
			if (num < 0 || 9 < num) {
				System.out.println("0から9の数値を入力して下さい。");
				continue;
			}
			total += num;

		}
	}

}
