package practice10_19;

public class Practice13 {
	public static void main(String[] args) {

		int[][] score = {
				{ 95, 88, 87 }, //阿部
				{ 70, 81, 100 }, //井上
				{ 12, 98, 90 } //上田
		};

		for (int x = 0; x < score.length; x++) {
			int total = 0;

			for (int y = 0; y < score[x].length; y++) {
				total += score[x][y];
			}
			System.out.println(total);

		}
		//拡張for文を使ったやり方
		for (int[] point : score) {
			int total = 0;
			for (int i : point) {
				total += i;

			}
			System.out.println(total);
		}

	}
}
