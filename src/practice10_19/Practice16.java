package practice10_19;

import practice.logics.PracticeLogic16;

public class Practice16 {
	public static void main(String[] args) {
		System.out.println("あなたの年齢を入力して下さい。");
		int age = new java.util.Scanner(System.in).nextInt();

		boolean result = PracticeLogic16.checkChild(age);

		if(result) {
			System.out.println("子供");
		}else {
			System.out.println("大人");
		}
	}

}
