package practice.practice17;

public class Main {
	public static void main(String[] args) {
		Employee a = new Employee();
		Employee b = new Employee("太郎");
		Employee c = new Employee("二郎", 30);

		System.out.println(a.getProfile());
		System.out.println(b.getProfile());
		System.out.println(c.getProfile());
	}
}
