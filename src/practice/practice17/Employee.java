package practice.practice17;

public class Employee {
	final static String company = "テストカンパニー"; //所属会社
	String name; //名前
	int age; //年齢

	Employee() {
		this("名無し");
	}

	Employee(String name) {
		this(name, 20);
	}

	Employee(String name, int age) {
		this.name = name;
		this.age = age;
	}

	String getProfile() {
		return "会社は" + this.company + "、" + "名前は" + this.name + "、" + "年齢は" + this.age;
	}

}
