package practice.practice18;

public class Main {
	public static void main(String[] args) {
		Employee employee = new Employee();

		employee.setCompany("テストカンパニー");
		employee.setName("山田太郎");
		employee.setAge(20);

		System.out.println("所属会社：" + employee.getCompany());
		System.out.println("名前：" + employee.getName());
		System.out.println("年齢：" + employee.getAge());
	}

}
